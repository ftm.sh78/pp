<?php

use App\Products;
use Illuminate\Database\Seeder;

class ProductsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
     for ($i=0;$i<10;$i++){
         \App\Product::create([
             'name'=>$faker->name,
             'des' => $faker->text,
             'img'=>$faker->url,
             'amount'=>11.5,
             'user_id'=>$faker->numberBetween(1,10),
             'created_at' => \Carbon\Carbon::now(),
             'updated_at' => \Carbon\Carbon::now(),

         ]);


     }
    }
}
