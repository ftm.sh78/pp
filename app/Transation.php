<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transation extends Model
{
   protected $fillable=[
       'amount',
       'user_id',
       'token',
       'Authority',
       'RefID',
       'status',
   ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
