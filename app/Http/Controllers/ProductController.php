<?php

namespace App\Http\Controllers;

use App\Product;
use App\Products;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::all();
        return view('products.show',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $validatedData = $request->validate([
//            'name' => 'required|max:255',
//            'des' => 'required',
//            'img'=>'mimes:jpeg,bmp,png',
//            'amount'=> 'required',
//        ]);

        $file=$request->file('img');

        $type=$file->getClientOriginalExtension();//png

        $product=  Product::create(
            [
                'name'=>$request->name,
                'des'=>$request->des,
                'img'=>'f',
                'amount'=>$request->amount,
                'user_id'=>1

            ]
        );
        $new_file_name=$product->id.'.'.$type;//10.png
        $product->update([
            'img'=>$new_file_name,
        ]);

        $adders=public_path('/files/imges/products');
        $file-> move($adders,$new_file_name);
        $request->session()->flash('success','محصول مورد نظر با موفقیت ثبت شد');
        return redirect(route('product.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $products=Product::all();

        return view('products.show',compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $products=Product::all();
        return view('products.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Product $product)
    {
        $file=$request->file('img');

        if(isset($file)){
            $type=$file->getClientOriginalExtension();//png
            $new_file_name=$product->id.".".$type;
            $adders=public_path('/files/images');
            $file-> move($adders,$new_file_name);
        }
        else{
            $new_file_name=$product->img;
        }




        $product->update([
            'name'=>$request->name,
            'des'=>$request->des,
            'img'=>$new_file_name,
            'amount'=>$request->amount,
        ]);



        $request->session()->flash('success','محصول مورد نظر با موفقیت edit شد');
        return redirect(route('product.show_all'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return back();
    }
    public function show_all(Product $product){
      $products=Product::all();
      return view('products.show_all',compact('products'));
    }
    public function information(Product $product){
     return view('products.information',compact('product'));
    }
}
