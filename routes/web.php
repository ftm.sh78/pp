<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::resource('product','ProductController');
Route::get('show_all','ProductController@show_all')->name('product.show_all');
Route::get('product/information/{product}','ProductController@information')->name('product.information');
Route::post('ckeditor/image_upload', 'CKEditorController@upload')->name('upload');
////////bank///////////////

Route::get('buy',function(){
    return view('shop');
});
Route::group(['prefix'=>'/','middleware'=>'Login'],function (){
    Route::get('order','siteController@order');
    Route::post('shop','siteController@add_order');
});



/////////end bank////////////


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
