<!DOCTYPE html>
<!--ترجمه شده توسط مرجع تخصصی برنامه نویسان-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no">
    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <title>سایت فروشگاهی</title>
   @include('partion-ghaleb.css.css')
    @yield('css')
</head>

<!--ترجمه شده توسط مرجع تخصصی برنامه نویسان-->
<body>
<div id="wrapper">
    <div class="wrapper-holder">
        <div class="header-holder">
            <header id="header">
                <span class="logo"><a href="index.html">Bambino</a></span>
                <div class="tools-nav_holder">
                    <ul class="tools-nav">
                        <li><a href="#">حساب کاربری</a></li>
                        <li class="login"><a href="#">خروج</a></li>
                    </ul>

                </div>

                <div class="clear"></div>
                <a class="menu_trigger" href="#">menu</a>
                <nav id="nav">
                    <ul class="navi">
                        <li class="searc_li" >
                            <div  class="ul_search li">
                                <a class="search" href="#"><span>جستجو</span></a>
                                <form method="get" class="searchform" action="#">
                                    <input type="text" class="field" name="s" id="s" placeholder="دنبال چی میگردی؟" />
                                    <input type="submit" class="submit" value=""  />
                                    <div class="clear"></div>
                                </form>
                            </div >
                        </li>
                       @include('partion-ghaleb.meno.meno1')
                    </ul>
                    <div  class="ul_search">
                       @include('partion-ghaleb.search.search')
                    </div >
                </nav>
            </header>
        </div>
        <!--ترجمه شده توسط مرجع تخصصی برنامه نویسان-->
        <section class="main">
            <div class="content">
                <div class="box_images">
                    <a href="#"><img src="/ghaleb/images/123.png" alt="" ></a>
                </div>
                <ul class="box_image_list">
                    <li><a href="#"><img src="/ghaleb/images/pic2.png" alt="" ></a></li>
                    <li><a href="#"><img src="/ghaleb/images/pic3.png" alt="" ></a></li>
                    <li><a href="#"><img src="/ghaleb/images/pic4.png" alt="" ></a></li>
                </ul>

                <div class="clear"></div>
                <section class="container">
                        <div class="slides">

                            @yield('meno2')

                        </div>
                @yield('car')
                    <!--ترجمه شده توسط مرجع تخصصی برنامه نویسان-->
                </section>


            </div>

        </section>

    </div>

    <footer id="footer">
        <div class="footer-content">
        @include('partion-ghaleb.footer.footer')
        </div>
    </footer>
</div>
<!--ترجمه شده توسط مرجع تخصصی برنامه نویسان-->
@include('partion-ghaleb.js.js')
@yield('js')
</body>
</html>
