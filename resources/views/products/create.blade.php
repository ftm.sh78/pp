@extends('layot')
@section('meno2')

    <form action="{{route('product.store')}}"method="post" enctype="multipart/form-data">

        @csrf
        <div class="form-group required">
            <label for="input-firstname" class="col-sm-2 control-label">نام محصول</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="input-firstname" placeholder="نام" value="" name="name">
            </div>
        </div>
        <div class="form-group required">
            <label for="input-email" class="col-sm-2 control-label">قیمت</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="input-lastname" placeholder="مقدار" value="" name="amount">
            </div>
        </div>
        <div class="form-group required">
            <label for="input-email" class="col-sm-2 control-label">آدرس ایمیل</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="input-email" placeholder="آدرس ایمیل" value="" name="email">
            </div>
        </div>

        <div class="form-group required">
            <label for="input-lastname" class="col-sm-2 control-label">توضیحات</label>
            <div class="col-sm-10">
                <textarea class="form-control" id="summary-ckeditor" name="des"></textarea>
            </div>
        </div>
        <div class="form-group required">
            <label for="input-telephone" class="col-sm-2 control-label">عکس محصول</label>
            <div class="col-sm-10">
                <input type="file" class="form-control" id="input-telephone" placeholder="شماره تلفن" value="" name="img">
            </div>
        </div>

        <input type="submit"value="create">

    </form>
@endsection
@section('js')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'summary-ckeditor', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    @endsection