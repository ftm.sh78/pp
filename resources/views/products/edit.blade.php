@extends('layot')
@section('meno2')

    <form action="{{route('product.update',['product'=>$product->id])}}" method="post" enctype="multipart/form-data">
        {{method_field('PATCH')}}
        @csrf
    <div class="form-group required">
        <label for="input-firstname" class="col-sm-2 control-label">نام محصول</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="input-firstname" placeholder="نام" value="{{$product->name}}" name="name">
        </div>
    </div>
    <div class="form-group required">
        <label for="input-email" class="col-sm-2 control-label">قیمت</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="input-lastname" placeholder="مقدار" value="{{$product->amount}}" name="amount">
        </div>
    </div>
    <div class="form-group required">
        <label for="input-email" class="col-sm-2 control-label">آدرس ایمیل</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="input-email" placeholder="آدرس ایمیل" value="" name="email">
        </div>
    </div>

    <div class="form-group required">
        <label for="input-email" class="col-sm-2 control-label" >توضیحات</label>
        <div class="col-sm-10">
           <textarea name="des" rows="#">
              {!!  $product->des  !!}
           </textarea>

        </div>
    </div>
    <div class="form-group required">
        <label for="input-email" class="col-sm-2 control-label">عکس</label>
        <img src="/files/images/{{$product->img}}" style="width: 50px;height: 50px ">
        <div class="col-sm-10">
            <input type="file" class="form-control" id="input-email" placeholder="آدرس ایمیل" value="" name="img">
        </div>
    </div>

<input type="submit"value="edit">

    </form>
    @endsection