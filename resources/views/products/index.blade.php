@extends('layot')
@section('car')
<div class="slides">
        <p>آخرین محصولات اضافه شده</p>
        <ul class="item-list">
        <?php $products=\App\Product::latest('id')->paginate(4); ?>
        @foreach($products as $product)
        <li class="last">
            <div class="item">
                <div class="image">
                    <a href="#"><img src="/ghaleb/images/{{$product->img}}" alt=""style="width: 200px;height: 200px"></a>
                    <div class="hover" style="color: black">
                        <p>{{$product->name }}</p>
                        <strong>{{$product->amount}}تومان</strong>
                    </div>
                </div>
            </div>
        </li>
            @endforeach
    </ul>
</div>
@endsection
