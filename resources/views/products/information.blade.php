@extends('layot')
@section('car')
<body>
<div id="wrapper">
    <div class="wrapper-holder">

        <section class="main">
            <div class="content">

                <div class="details-info">
                    <div class="slid_box">
                        <ul class="bxslider">
                            <li><img src="/files/images/{{$product->img}}" /></li>
                            <li><img src="/files/images/{{$product->img}}" /></li>
                            <li><img src="/files/images/{{$product->img}}" /></li>
                            <li><img src="/files/images/{{$product->img}}" /></li>
                        </ul>
                        <div id="bx-pager">
                            <a class="first" data-slide-index="0" href="#"><img src="/files/images/{{$product->img}}" /></a>

                        </div>

                    </div>
                    <div class="description">
                        <div class="head">
                            <h1 class="title">نام محصول</h1>

                            <h2>{{$product->name}}</h2>
                        </div>
                        <div class="section">
                            <form class="form-sort page" action="#">
                                <fieldset>
                                    <div class="row">
                                        <label for="page">تعداد:</label>
                                        <select id="page">
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                        <div class="clear"></div>
                                    </div>
                                    <form method="post" action="{{url('shop')}}">
                                        @csrf
                                        <input type="text" name="price" value="100">
                                        <button type="submit">تکمیل خرید</button>
                                    </form>
                                    {{--<button type="button" id="button-cart" class="btn btn-primary btn-lg">افزودن به سبد</button>--}}
                                </fieldset>
                            </form>
                        </div>
                        <div id="tabs">

                            <h1 class="title">توضیحات</h1>
                            <div id="tabs-2">{!!  $product->des!!}</div>

                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </section>
    </div>
    <!--ترجمه شده توسط مرجع تخصصی برنامه نویسان-->

</div>
<!--ترجمه شده توسط مرجع تخصصی برنامه نویسان-->

</body>
</html>
    @endsection


