@extends('layot')
@section('meno2')
    <div class="content" >
        <table class="list_table">
            <tbody><tr>
                <td class="braun first">
                    <span>مورد</span>
                </td>
                <td class="braun price">
                    <span>قیمت</span>
                </td><td class="braun price">
                    <span>حذف</span>
                </td><td class="braun price">
                    <span>ویرایش</span>
                </td>

            </tr>
            <tr>
                @foreach($products as $product)
                <td class="white first">
                    <img src="/files/images/{{$product->img}}" alt="" width="90" height="131">
                    <div class="description">
                        <h3><a href="#">

                                {{$product->name}} </a></h3>
                        <p>{!! $product->des !!}</p>
                    </div>
                </td>
                <td class="white two">{{$product->amount}}</td>


                <td class="white last"><div class="row"><a class="btn-delete" href="#">حذف</a></div>

                    <form action="{{route('product.destroy',['product'=>$product])}}" method="post" >
                        @csrf
                        {{method_field('DELETE')}}
                        <input type="submit" value="delet">
                    </form>

                </td>
                    <td class="white last"><div class="row"><a class="btn-delet" href="{{route('product.edit',['product'=>$product->id])}}">
                                ویرایش</a></div>

                    </td>
            </tr>
@endforeach
            </tbody>
        </table>



    </div>
    @endsection