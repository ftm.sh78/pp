@extends('layot')
@section('car')
    <div class="slides">
        <section class="bar">
            <div class="bar-frame">
                <ul class="breadcrumbs">
                    <li><a href="index.html">صفحه اصلی</a></li>
                    <li>نتایج محصول</li>
                </ul>
            </div>
       </section>

        <ul class="item-list">
        @foreach($products as $product)
        <ul class="item-product">
            <li>
                <div class="item">
                    <div class="image">
                        <a href="products.show"><img src="/files/images/{{$product->img}}" alt=""></a>
                    </div>
                    <span class="name"><a href="products.show">

                         {{$product->name}}</a></span>
                    <span>{{$product->amount}}تومان</span>
                    <div class="button-group">
                        <a href="{{route('product.information',['product'=>$product->id])}}">
                            <button class="btn-primary" type="button" onClick="cart.add('42');"><span> افزودن به سبد خرید</span></button>
                        </a>
                </div>
                </div>
            </li>

        </ul>
        @endforeach
        </ul>

    </div>
    @endsection


